import csv
import json

def read_data():
	with open('rainfall.csv', 'r') as read_rain:
		rainfall = read_rain.readlines()
		read_rain.close()
		rain = []
		for line in rainfall[1:]:
			rain1 =	line.split(',')
			rain.append({
				"id" : int(rain1[0]), "year" : int(rain1[1]), "rain" : float(rain1[2])
			})
	return rain 

def dates(data, start=None, end=None):

	x = []	
	
	if start is None and end is None:
		for year in data:
			x.append(year)
	
	elif start is not None and end is not None:
		for year in data:
			if year['year'] >= start and year['year'] <= end:
				x.append(year)

	elif start is not None:
		for year in data:
			if year['year'] >= start:
				x.append(year)

	elif end is not None:
		for year in data:
			if year['year'] <= end:
				x.append(year)

	return x
def paginate(data, offset=None, limit=None):

	offset = offset
	limit = limit

	if limit is None and offset is None:
		return data
	
	elif offset is not None and limit is not None:
		limit = limit + 1
		return data[offset:limit]

	elif offset is not None:
		return data[offset:]

	elif limit is not None:
		return data[:limit]
	


